package com.MavenSelenium;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class FindAllLinksInWebpage {
	 WebDriver driver;

		@Test
		public void getAllLinks() throws InterruptedException  {
			System.setProperty("webdriver.chrome.driver", "C:\\myworkspace\\MavenSelenium\\drivers\\chromedriver.exe");
			driver= new ChromeDriver();
			driver.get("https://money.rediff.com/gainers/bsc/daily/groupa");
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			List<WebElement> links = driver.findElements(By.xpath("//a"));
			System.out.println(links.size());
			
			ArrayList<String> array = new ArrayList<String>();
			
			for(int i = 0; i<links.size(); i++) {
				System.out.println(links.get(i).getText());
				array.add(links.get(i).getAttribute("href"));
				System.out.println(links.get(i).getAttribute("href"));
				
				}
			System.out.println(array.size());
}
}