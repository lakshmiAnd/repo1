package com.MavenSelenium;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class ScrollUpAndDownExample {
	
	WebDriver driver;
	
	@Test
	public void scrollInSelenium() throws InterruptedException {
	
	System.setProperty("webdriver.chrome.driver", "C:\\myworkspace\\MavenSelenium\\drivers\\chromedriver.exe");
	driver = new ChromeDriver();
	driver.get("https://www.tirerack.com/content/tirerack/desktop/en/homepage.html");
	Thread.sleep(3000);
	((JavascriptExecutor)driver).executeScript("window.scrollTo(0,document.body.scrollHeight)");
	Thread.sleep(3000);
	((JavascriptExecutor)driver).executeScript("window.scrollTo(0,-document.body.scrollHeight)");
	Thread.sleep(3000);
	((JavascriptExecutor)driver).executeScript("window.scrollBy(0,1500)");
	Thread.sleep(3000);
	((JavascriptExecutor)driver).executeScript("window.scrollBy(0,-1500)");
	Thread.sleep(3000);
	((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath("//*[@class='whiteRdBtn']")));
	
}
}