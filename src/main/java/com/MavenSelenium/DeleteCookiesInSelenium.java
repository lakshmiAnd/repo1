package com.MavenSelenium;

import java.util.Set;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class DeleteCookiesInSelenium {
	
	WebDriver driver;
	
	@Test
	public void deleteCookies() throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "C:\\myworkspace\\MavenSelenium\\drivers\\chromedriver.exe");
		driver  = new ChromeDriver();
		driver.navigate().to("http://flipkart.com/");
		
		/*driver.manage().deleteCookieNamed("gpv_pn");
		
		//after adding the cookie we will check that by displaying all the cookies.
		Set<Cookie> coolieslist = driver.manage().getCookies();
         for (Cookie getcookies :coolieslist) {
        	 System.out.println(getcookies);
         }
        */
         //for delete all cookies
         driver.manage().deleteAllCookies();
 		
 		 		Set<Cookie> coolieslist = driver.manage().getCookies();
          for (Cookie getcookies :coolieslist) {
         	 System.out.println(getcookies);
          }
         
}
}