package com.MavenSelenium;

import java.util.Set;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class AddCookieInSelenium {
WebDriver driver;
	
	@Test
	public void addCookies() throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "C:\\myworkspace\\MavenSelenium\\drivers\\chromedriver.exe");
		driver  = new ChromeDriver();
		driver.navigate().to("http://www.flipkart.com/");
		
	
		Cookie cookie = new Cookie("customCookie", "123454321");
		
		driver.manage().addCookie(cookie);
		
		Set<Cookie> cookies = driver.manage().getCookies();
		for(Cookie cooky : cookies) {
			System.out.println(cooky);
			System.out.println(cookie);
		}
			

}
}