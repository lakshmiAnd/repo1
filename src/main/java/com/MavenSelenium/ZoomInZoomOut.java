package com.MavenSelenium;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class ZoomInZoomOut {
	WebDriver driver;
	
	@Test
	public void zoomInZoomOut() throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "C:\\myworkspace\\MavenSelenium\\drivers\\chromedriver.exe");
		driver  = new ChromeDriver();
		
		driver.get("http://automationpractice.com/index.php");
		
		((JavascriptExecutor)driver).executeScript("document.body.style.zoom='40%'");
		Thread.sleep(3000);
		((JavascriptExecutor)driver).executeScript("document.body.style.zoom='100%'");
	}

}
