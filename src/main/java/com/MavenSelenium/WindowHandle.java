package com.MavenSelenium;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class WindowHandle {
 
	WebDriver driver;
	@Test
	
	public void windowHandle() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\myworkspace\\MavenSelenium\\drivers\\chromedriver.exe");
		driver= new ChromeDriver();
		driver.get("http://automationpractice.com/index.php");
		driver.findElement(By.xpath("//*[@id=\"social_block\"]/ul/li[3]/a")).click();//click youtube icon
	    Set<String> windowsId = driver.getWindowHandles(); //to get windows id
		Iterator<String> itr = windowsId.iterator();
		System.out.println("lenth of set object"+windowsId.size());
		System.out.println("lenth of iterator to sring ::::"+itr.toString());
		String parentId = itr.next();
		System.out.println("Parent id::::"+parentId);
		String childId = itr.next();
		System.out.println("childId::"+childId);
		Thread.sleep(10000);
		driver.switchTo().window(childId);
		System.out.println("child window ttle:::"+driver.getTitle());
		driver.close();
		driver.switchTo().window(parentId);
		System.out.println("parent window ttle:::"+driver.getTitle());
		Thread.sleep(10000);
		driver.findElement(By.xpath("//*[@id=\"footer\"]/div/section[2]/div/div/ul/li/a")).click();
			
		}
	}


