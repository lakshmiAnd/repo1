package com.MavenSelenium;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class GetRowsAndColmsFromWebTable {
	WebDriver driver;
	
	@Test
	
	public void getRowAndColumSize() throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "C:\\myworkspace\\MavenSelenium\\drivers\\chromedriver.exe");
		driver= new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.get("https://money.rediff.com/gainers/bsc/daily/groupa");
		int rows = driver.findElements(By.xpath("//*[@class=\"dataTable\"]/tbody/tr")).size();
		int columns = driver.findElements(By.xpath("//*[@class=\"dataTable\"]/tbody/tr[1]/td")).size();
		System.out.println("Number of rows are....>>>>"+rows);
		System.out.println("Number of columns are....>>>>"+columns);
	}
	@Test
	public void getRowData() {
		String rowData = driver.findElement(By.xpath("//*[@id=\"leftcontainer\"]/table/tbody/tr[10]/td[4]")).getText();//this xpath can change any time if the table rows change frequently 
		System.out.println("row data is:>>>>>"+rowData);
		 rowData = driver.findElement(By.xpath("//a[contains(text(),'Bajaj Auto Ltd.')]/parent::*/following-sibling::td[3]")).getText();//this is another way to write xpath related xpath  
		System.out.println("row data is:>>>>>"+rowData);
}
	
	}
